/*
    1. Declare 3 variables without initialization called username,password and role.
*/

/*
    
    2. Create a login function which is able to prompt the user to provide their username, password and role.
        -use prompt() and update the username,password and role global variable with the prompt() returned values.
        -add an if statement to check if the the username is an empty string or null or if the password is an empty string or null or if the role is an empty string or null.
            -if it is, show an alert to inform the user that their input should not be empty.
        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
                -if the user's role is admin, show an alert with the following message:
                    "Welcome back to the class portal, admin!"
                -if the user's role is teacher, show an alert with the following message:
                    "Thank you for logging in, teacher!"
                -if the user's role is a rookie, show an alert with the following message:
                    "Welcome to the class portal, student!"
                -if the user's role does not fall under any of the cases, as a default, show a message:
                    "Role out of range."
*/

	let userName = prompt("Please enter your name!");
	console.log(userName);
	let	passWord = prompt("Please enter your password!");
	console.log(passWord);
	let	role = prompt("Please enter your role!").toLowerCase();

	if (userName === '' || passWord === '' || role === ''){
		alert("Input must not be empty!!")
	} else {
		alert("Hello, " + role);

		switch (role) {
			case 'admin':
				alert('Welcome back to the class portal, admin');
				break;
			case 'teacher':
				alert('Thank you for logging in, teacher');
				break;
			case 'student':
				alert('Thank you for logging in, student');
				break;
			default:
				alert('Role is out of range');
				break;
		}
	}

	if (role === 'student'){

				alert("Welcome, Student! Lets calculate your average");

				function aveTotal(numA,numB,numC,numD){

				let ava = (numA + numB + numC + numD) / 4;
				let average = Math.round(ava);
				console.log(average);

					if (average <= 74) {
						console.log('Hello, student, your average is ' + average + '. The letter equivalent is F');
					} else if (average <= 80) {
						console.log('Hello, student, your average is ' + average + '. The letter equivalent is D');
					} else if (average <= 85) {
						console.log('Hello, student, your average is ' + average + '. The letter equivalent is C');
					} else if (average <= 90) {
						console.log('Hello, student, your average is ' + average + '. The letter equivalent is B');
					} else if (average <= 96) {
						console.log('Hello, student, your average is ' + average + '. The letter equivalent is A');
					} else if (average >= 97){
						console.log('Hello, student, your average is ' + average + '. The letter equivalent is A+');
					} else {
						console.warn('Invalid/Null data');
					}
				}
				
				let numA = parseInt(prompt("Enter your 1st Grade"));
				let numB = parseInt(prompt("Enter your 2nd Grade"));
				let numC = parseInt(prompt("Enter your 3rd Grade"));
				let numD = parseInt(prompt("Enter your 4th Grade"));
				aveTotal(numA,numB,numC,numD);

	} else {

		console.warn(role + '. You are not allowed to access this feature!')
	}


/*
    3. Create a function which is able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
        -add parameters appropriate to describe the arguments.
        -create a new function scoped variable called average.
        -calculate the average of the 4 number inputs and store it in the variable average.
        -research the use of Math.round() and round off the value of the average variable.
            -update the average variable with the use of Math.round()
            -console.log() the average variable to check if it is rounding off first.
        -add an if statement to check if the value of average is less than or equal to 74.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is F"
        -add an else if statement to check if the value of average is greater than or equal to 75 and if average is less than or equal to 79.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is D"
        -add an else if statement to check if the value of average is greater than or equal to 80 and if average is less than or equal to 84.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is C"
        -add an else if statement to check if the value of average is greater than or equal to 85 and if average is less than or equal to 89.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is B"
        -add an else if statement to check if the value of average is greater than or equal to 90 and if average is less than or equal to 95.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is A"
        -add an else if statement to check if the value of average is greater than 96.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is A+"

        Invoke and add a number as argument using the browser console.

        Stretch goal:
            -Add an if statement to check the role of the user.
                -if the role of the user is currently "teacher" or "admin" or undefined or null, show an alert:
                "<role>! You are not allowed to access this feature!".
                -else, continue to evaluate the letter equivalent of the student's average.

*/




			function showIntensityAlert(windSpeed){
				try{
					console.log("No Error Here!");
					alerat(determineTyphoonIntensity(windSpeed));
				} catch (error) {
					console.log(typeof error);

					// 'error.message' is used to access the information to an error object.
					console.warn(error.message);
				} finally {

					// Continue execution of code regardless of success and failure of code execution in the 'try' block to handle resolve errors. 
					alert('Intensity updates will show new alert');
					console.log('Hellow!');
				}
			}